#!/usr/local/bin/bash
# Based off of the documentation at https://www.server-world.info/en/note?os=FreeBSD_14&p=buildah&f=2

# create an empty container with [scratch]
newcontainer=$(buildah from scratch)
# mount [scratch] container
scratchmnt=$(buildah mount $newcontainer)

# install Base system
fetch https://download.freebsd.org/releases/amd64/14.1-RELEASE/base.txz
fetch https://download.freebsd.org/releases/amd64/14.1-RELEASE/lib32.txz
tar zxf base.txz -C $scratchmnt
tar zxf lib32.txz -C $scratchmnt
rm base.txz lib32.txz

# unmount the [scratch] container
buildah umount $newcontainer

# commit the image
buildah commit $newcontainer invent-registry.kde.org/sysadmin/ci-images/freebsd14-base:latest
